package main

import (
	"gitlab.com/pllm/http_demo/internal/handler"
	"log"
	"net/http"
)

func main() {

	http.HandleFunc("/create", handler.CreateUser)
	http.HandleFunc("/get", handler.GetUser)
	http.HandleFunc("/getall", handler.GetAllUsers)
	http.HandleFunc("/delete", handler.DeleteUser)
	log.Println("Starting server...")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
