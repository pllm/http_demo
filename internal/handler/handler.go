package handler

import (
	"encoding/json"
	"fmt"
	"gitlab.com/pllm/http_demo/internal/domain"
	"log"
	"net/http"
	"strconv"
)

type UserStore map[int]domain.User

var userStore = make(UserStore)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	var newUser domain.User
	defer r.Body.Close()

	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	newId := len(userStore) + 1
	newUser.Id = newId

	userStore[newId] = newUser

	w.WriteHeader(http.StatusCreated)
	message := fmt.Sprintf("User id:%d, name:%s, role: %s, created", newUser.Id, newUser.Name, newUser.Role)
	w.Write([]byte(message))
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	idParam := r.URL.Query().Get("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	user, ok := userStore[id]
	log.Println(user)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		errMsg := fmt.Sprintf("User with id %d does not exist", id)
		w.Write([]byte(errMsg))
		return
	}

	respBody, err := json.Marshal(&user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(respBody)
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	users := userStore
	err := json.NewEncoder(w).Encode(users)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	idParam := r.URL.Query().Get("id")
	userId, err := strconv.Atoi(idParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	for idx, user := range userStore {
		if user.Id == userId {
			delete(userStore, idx)
			w.Write([]byte(fmt.Sprintf("User with id %d has been deleted", userId)))
		}
	}
	w.WriteHeader(http.StatusOK)
}
